#!/bin/sh

# This script assumes it is being run on Centos 6.
# It should be run once to setup the machine.

# Turn off firewall - yeah not good practice but this is a demo only.
service iptables stop

# Pre-requisities
yum install -y wget unzip git java-1.6.0-openjdk-devel httpd urw-fonts

# Setup Jenkins
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
yum install -y jenkins-1.518-1.1

# Setup JBoss AS 7 (only for Aquillian integration tests, grrrrr....)
wget -q http://download.jboss.org/jbossas/7.1/jboss-as-7.1.1.Final/jboss-as-7.1.1.Final.zip
unzip jboss-as-7.1.1.Final.zip
mv jboss-as-7.1.1.Final /opt
sed -i 's/port="8080"/port="8081"/' /opt/jboss-as-7.1.1.Final/standalone/configuration/standalone.xml
chown -R jenkins:jenkins /opt/jboss-as-7.1.1.Final

# Copy CI box SSH keys for bitbucket access
mkdir $HOME/.ssh
cp /vagrant/keys/id_rsa.ci $HOME/.ssh/id_rsa
cp /vagrant/keys/id_rsa.pub.ci $HOME/.ssh/id_rsa.pub
cp /vagrant/keys/known_hosts.ci $HOME/.ssh/known_hosts
chmod 700 $HOME/.ssh
chmod 600 $HOME/.ssh/id_rsa
chmod 600 $HOME/.ssh/id_rsa.pub

# Clone Jenkins configuration from git repo
service jenkins stop
rm -rf /var/lib/jenkins/*
cd /var/lib/jenkins
git clone git@bitbucket.org:clarenceb/cdtt-ci-brownbag-jenkins.git .
chown -R jenkins:jenkins .

mkdir -p .ssh
cp /vagrant/keys/id_rsa.ci .ssh/id_rsa
cp /vagrant/keys/id_rsa.pub.ci .ssh/id_rsa.pub
cp /vagrant/keys/known_hosts.ci .ssh/known_hosts
chmod 700 .ssh
chmod 600 .ssh/id_rsa
chmod 600 .ssh/id_rsa.pub
chown -R jenkins:jenkins .ssh

echo "JENKINS_JAVA_OPTIONS=\"$JENKINS_JAVA_OPTIONS -Dhudson.model.Api.INSECURE=true -Djava.awt.headless=true\"" >> /etc/sysconfig/jenkins
git clone git://github.com/tuo/jenkins-monitor.git /var/www/html/monitor
cp /var/www/html/monitor/conf/config.js{.sample,}
sed -i 's/ci.jruby.org\/view\/Ruboto/localhost:8080/g' /var/www/html/monitor/conf/config.js

service jenkins start
service httpd start
