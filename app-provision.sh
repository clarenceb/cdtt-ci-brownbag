#!/bin/sh

# This script assumes it is being run on Centos 6.
# It should be run once to setup the machine.

# Turn off firewall - yeah not good practice but this is a demo only.
service iptables stop

# Pre-requisities
yum install -y wget unzip java-1.6.0-openjdk

# Set up JBoss AS 7
wget http://download.jboss.org/jbossas/7.1/jboss-as-7.1.1.Final/jboss-as-7.1.1.Final.zip
unzip jboss-as-7.1.1.Final.zip
mv jboss-as-7.1.1.Final /opt

cd /opt/jboss-as-7.1.1.Final

# Change JBoss AS 7 default port
sed -i 's/127\.0\.0\.1/192.168.33.11/' standalone/configuration/standalone.xml

# Run JBoss in the background (its a manual step for now)
# Note: Cannot do this from here at it will prevent `vagrant up` from returning.
echo "Please run the following command as root on the app server: nohup /opt/jboss-as-7.1.1.Final/bin/standalone.sh &"
