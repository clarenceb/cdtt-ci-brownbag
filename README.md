About this CI Brownbag Demo
===========================
This is a demo project for a CI demonstration.  You get 2 VMs created for you using Vagrant: Jenkins and an Application.
It relies on a application source repo that is currently hardcoded to my one on bitbucket but it could be parameterised.
If you trigger the `commit` build and it passes then the `deploy` stage runs and deploys to the app VM.

The dependent repos are:

`https://bitbucket.org/clarenceb/cdtt-ci-brownbag-jenkins`

This is used when provisioning the Jenkins VM from scratch - it hold all the necessary configuration (e.g. jobs and plugins).

`https://bitbucket.org/clarenceb/cdtt-ci-brownbag-app`

This is a JBoss AS 7 quickstart app which the Jenkins CI monitors and builds and deploys to the app server VM.
For the brown bag, I make the commit stage fail by change the implementation so that the Aquillian integration test fails.

If you want to use this CI demo for your own purposes you can fork the repo and update the necessary git repo URLs to point to your fork.
In this case, you would have to update the SSH keys in the `keys` directory or add the existing public key to the `Deployment Keys` of your forked repo(s).

Since building the 2 VMs takes some time, I suggest building them before hand, testing that the CI and App VMs work and then suspending them with `vagrant suspend` for demonstrating later.  I also recommend installing the `vagrant-cachier` plugin so that any retrieved assets are cached locally on your host so that rebuild the VMs a second time will be faster. 

I've only tested this on Mac OS X 10.8.3 but in theory it should work on any system with Vagrant (and Virtualbox) running.

*Note*: This is supposed to be a simple demo for a brown bag and not intended to illustrate best practices for a production system!  Do not use this for any purpose other than a demo.  Provisioning does not use anything fancy like puppet or chef; it uses bash scripts.


Pre-requisities:
================

* Virtualbox 4.2.x+
* Vagrant 1.2.x 

Steps:
======

* Download and install Virtualbox for your OS (https://www.virtualbox.org/wiki/Downloads)
* Download and install Vagrant for your OS (http://downloads.vagrantup.com/)
* (optional) Install the `vagrant-cachier` plugin to cache RPMs required to build the VMs: `vagrant plugin install vagrant-cachier`
* `vagrant box add CentOS-6.3-x86_64-minimal.box https://dl.dropbox.com/u/7225008/Vagrant/CentOS-6.3-x86_64-minimal.box` to add the required Vagrant base box 
* `vagrant up` to create and start up the VMs
* Start the JBoss app server (must be done manually after creating the VMs since there is no service script for JBoss AS 7):
	- `vagrant ssh app`
	- `sudo su -`
	- `nohup /opt/jboss-as-7.1.1.Final/bin/standalone.sh &`
	- `exit`
	- `exit`

Accessing the Jenkins:
======================

* SSH access: `vagrant ssh ci` (Jenkins home dir is `/var/lib/jenkins` and config file is `/etc/sysconfig/jenkins`)
* Web access to Jenkins master console: Browse to `http://localhost:8080` on the host machine
* Web access to the build dashboard: Browse to `http://localhost:8090/monitor`

Accessing the application:
==========================

* SSH access: `vagrant ssh app` (JBoss AS 7 is installing in /opt/jboss-as-7.1.1.Final)
* Web access to the app: Browse to `http://192.168.33.11:8080/jboss-as-kitchensink` from the host machine
* Web access the JBoss main console: Browse to `http://192.168.33.11:8080` from the host machine
